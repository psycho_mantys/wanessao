#!/usr/bin/env python

import physics
import camera
import GIFImage
import group

import stage
import character

from utils import *
from colors import *

class Stage(object):
	def __init__(self, game_loop, config):
		self.game_loop=game_loop
		self.config=config

		self.physics=physics.Physics(self.config)

		tile=character.NOPLAYER["tile"]

		self.camera = camera.Camera(camera.complex_camera, self.config.screen_width, self.config.screen_height, self.config.screen_width, self.config.screen_height)

		self.player=[]
		self.player.append(character.PLAYER["haggar"].Player(self.config, self.camera, 10, 200))
		self.player[0].id='Player00'

		self.player.append(character.PLAYER["haggar"].Player(self.config, self.camera, 650, 10))
		self.player[1].id='Player01'

		self.layer_front=group.Group()
		#self.layer_front=pygame.sprite.Group()
		self.layer_middle=group.Group()
		#self.layer_middle=pygame.sprite.Group()
		self.layer_background=group.Group()
		#self.layer_background=pygame.sprite.Group()
		wall=[]
		wall.append(tile.Tile(self.config, self.physics, self.camera, rfn("media/img/blank_field.png"), 0, 350, (0,0)))
		wall[0].id='Floor00'
		wall[0].group='static'
		self.layer_front.add(wall[0])
		self.physics.add(parent=wall[0], apply_gravity=(False, False))


		self.layer_middle.add(self.player[0])
		#self.physics.add(parent=self.player[0], apply_gravity=(False, False))
		self.physics.add(parent=self.player[0])
		self.layer_middle.add(self.player[1])
		self.physics.add(parent=self.player[1])
		#self.physics.add(parent=self.player[1], apply_gravity=(False, False))
	
		self.background=GIFImage.GIFImage(rfn("media/img/stage_00.gif"))
		self.layer_background.add(self.background)

		#self.upbar=upbar.Upbar(config, self.player)
		#self.layer_front.add(self.upbar)

		#self.layer_front.print()

	def __del__(self):
		dprint=self.config.debug.logger.debug
		dprint("Stage end")

	def event_handler(self):
		for event in pygame.event.get():
			# Trata evento QUIT
			if event.type==pygame.QUIT:
				self.game_loop.done=True
			elif event.type==pygame.KEYDOWN:
				print(pygame.key.name(event.key))
				print(event.key)
				if event.key==pygame.K_ESCAPE:
					self.game_loop.done=True
				if event.key==pygame.K_LEFT:
					self.player[0].left()
				if event.key==pygame.K_RIGHT:
					self.player[0].right()
				if event.key==pygame.K_DOWN:
					self.player[0].down()
				if event.key==pygame.K_UP:
					self.player[0].up()
				if event.key==pygame.K_SPACE:
					self.player[0].jump()
				# Teste para mudar fase
				if event.key==pygame.K_r:
					self.game_loop.stage=stage.STAGE["basic"].Stage(self.game_loop, self.config)
				if event.key==pygame.K_z:
					self.player[0].attack(self.player[1])

	def game_logic(self, dt):
		#self.camera.update(pygame.Rect( ((self.player[0].x-self.player[1].x)/2, (self.player[0].y-self.player[1].y)/2), (1,1)) )

		# Coloca os players de frente para o outro
		if self.player[0].rect.center[0]<self.player[1].rect.center[0]:
			self.player[0].side='right'
			self.player[1].side='left'
		else:
			self.player[0].side='left'
			self.player[1].side='right'

		self.camera.update(self.player[0])

	def step_physics(self, dt):
		self.physics.apply_velocity(dt)
		self.physics.apply_gravity(dt)
		self.physics.apply_collision()

		self.layer_background.update(dt)
		self.layer_middle.update(dt)
		self.layer_front.update(dt)

	def render(self, screen):
		screen.fill(COLOR.black)
		#screen.fill(COLOR.pink)

		self.layer_background.draw(screen)
		self.layer_middle.draw(screen)
		self.layer_front.draw(screen)

		# Debug
		self.config.debug.pygame_show_grid(screen)
		self.config.debug.show_fps(self.game_loop.clock)

		#pygame.draw.line(self.screen, COLOR.yellow, self.player[0].rect.center, self.player[1].rect.center, 3)

		pygame.display.flip()


