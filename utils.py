#!/usr/bin/env python

import pygame
from colors import *

def rfn(filename):
	from pkg_resources import resource_filename
	return resource_filename(__name__, filename)


def static_vars(**kwargs):
	""" Define variaveis estaticas """
	def decorate(func):
		for k in kwargs:
			setattr(func, k, kwargs[k])
		return func
	return decorate

class Point:
	def __init__(self, x=0, y=0):
		self.x=x
		self.y=y

