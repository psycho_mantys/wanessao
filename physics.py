#!/usr/bin/env python

import pygame

class Body(object):
	def __init__(self, x=0.0, y=0.0, velocity=(0,0)):

		self.y=y
		self.x=x

		self.id=None
		self.group=None

		self.velocity=velocity
		#self.physics.add(parent=self)

	def move(self, x=0, y=0):
		self.rect.move_ip(x, y)
		self.x=self.rect.x
		self.y=self.rect.y

	def dt_x_move(self, dt):
		dx=dt*(self.velocity[0]/1000)
		self.x+=dx
		self.rect.x=self.x


	def dt_y_move(self, dt):
		dy=dt*(self.velocity[1]/1000)
		self.y+=dy
		self.rect.y=self.y


	def dt_move(self, dt):
		self.dt_x_move(dt)
		self.dt_y_move(dt)



class Physics(object):
	def __init__(self, config):
		self.body_list=[]
		self.gravity=(0,100)
		self.config=config

	def add(self, **args):
		self.body_list.append(args)

	def apply_gravity(self, dt):
		for body in self.body_list:

			set_gravity=body.get('apply_gravity', (True, True))

			velocity_x=body['parent'].velocity[0]
			if set_gravity[0]:
				velocity_x=body['parent'].velocity[0]+(dt*self.gravity[0]/1000)
			velocity_y=body['parent'].velocity[1]
			if set_gravity[1]:
				velocity_y=body['parent'].velocity[1]+(dt*self.gravity[1]/1000)
			body['parent'].velocity=(velocity_x, velocity_y)

	def apply_velocity(self, dt):
		for body in self.body_list:
			if body.get('apply_velocity', True):
				body['parent'].dt_move(dt)


	def apply_collision(self):
		for index1, body1 in enumerate(self.body_list):
			for index2, body2 in enumerate(self.body_list[index1+1:]):
				if body1['parent'].rect.colliderect(body2['parent'].rect) and body1['parent'].id!=body2['parent'].id:
					self.collide(body1['parent'],body2['parent'])
	
	def in_collision(self, sprite):
		for body in self.body_list:
			if sprite.rect.colliderect(body['parent'].rect) and sprite.id!=body['parent'].id:
				return body['parent']

		return False


	def collide(self, body1, body2):
		if all([body1.group=='player',body2.group=='player']):
			self.collide_player_x_player(body1, body2)
		elif body1.group=='static':
			self.collide_static_x_any(body1, body2)
		elif body2.group=='static':
			self.collide_static_x_any(body2, body1)
		else:
			self.collide_any_x_any(body1, body2)

	def collide_static_x_any(self, body1, body2):
		dprint=self.config.debug.logger.debug

		body1rect=pygame.Rect( (body1.rect.left, body1.rect.top) , (body1.rect.w, body1.rect.h) )
		body2rect=pygame.Rect( (body2.rect.left, body2.rect.top) , (body2.rect.w, body2.rect.h) )

		contact_surface_x=(body1rect.w+body2rect.w-abs(body1rect.left-body2rect.left)-abs(body1rect.right-body2rect.right))/2
		contact_surface_y=(body1rect.h+body2rect.h-abs(body1rect.bottom-body2rect.bottom)-abs(body1rect.top-body2rect.top))/2

		dprint("Colision(SA): {} - {} ==> Dx: {} - Dy: {} - ".format(body1.id, body2.id, contact_surface_x, contact_surface_y))

		# Em X
		if contact_surface_x<contact_surface_y:
			dprint("Colisão em X")
			if   body1rect.left<body2rect.left and body1rect.right<body2rect.right:
				#dprint("DOWN!!")
				body2rect.left=body1rect.right
				body2.x=body2rect.x
			elif body1rect.left<body2rect.right and body2rect.right<body1rect.right:
				#dprint("UP!!")
				body2rect.right=body1rect.left
				body2.x=body2rect.x

			body2.velocity=(0, body2.velocity[1])
		# Em Y
		elif contact_surface_x>contact_surface_y:
			dprint("Colisão em Y")
			if   body1rect.y<body2rect.y:
				#dprint("DOWN!!")
				body2rect.top=body1rect.bottom
				body2.y=body2rect.y
			elif body1rect.bottom>=body2rect.bottom:
				#dprint("UP!!")
				body2rect.bottom=body1.rect.top+1
				body2.y=body2rect.y
			body2.velocity=(body2.velocity[0], 0)

	def collide_player_x_player(self, body1, body2):
		dprint=self.config.debug.logger.debug

		body1rect=pygame.Rect( (body1.rect.left, body1.rect.top) , (body1.rect.w, body1.rect.h) )
		body2rect=pygame.Rect( (body2.rect.left, body2.rect.top) , (body2.rect.w, body2.rect.h) )

		contact_surface_x=(body1rect.w+body2rect.w-abs(body1rect.left-body2rect.left)-abs(body1rect.right-body2rect.right))/2
		contact_surface_y=(body1rect.h+body2rect.h-abs(body1rect.bottom-body2rect.bottom)-abs(body1rect.top-body2rect.top))/2

		dprint("Colision: {} - {} ==> Dx: {} - Dy: {} - ".format(body1.id, body2.id, contact_surface_x, contact_surface_y))

		# Em X
		if contact_surface_x<contact_surface_y:
			#print("Colisão em X")
			if   body1rect.left<body2rect.left and body1rect.right<body2rect.right:
				#print("DOWN!!")
				body2rect.left=body1rect.right
				body2.x=body2rect.x

				body1.velocity=(0, body1.velocity[1])
				body2.velocity=(0, body2.velocity[1])
			elif body1rect.left<body2rect.right and body2rect.right<body1rect.right:
				#print("UP!!")
				body2rect.right=body1rect.left
				body2.x=body2rect.x

				body1.velocity=(0, body1.velocity[1])
				body2.velocity=(0, body2.velocity[1])
		# Em Y
		elif contact_surface_x>contact_surface_y:
			#print("Colisão em Y")
			if   body1rect.top>body2rect.top:
				if body2rect.center[0]<body1rect.center[0]:
					body2rect.right=body1rect.left
				else:
					body2rect.left=body1rect.right
				body2.x=body2rect.x

				#body1.velocity=(body1.velocity[0], 0)
				#body2.velocity=(body2.velocity[0], 0)
			else:
				#dprint("UP!!")
				if body2rect.center[0]<body1rect.center[0]:
					body1rect.left=body2rect.right
				else:
					body1rect.right=body2rect.left
				body1.x=body1rect.x

	def collide_any_x_any(self, body1, body2):
		dprint=self.config.debug.logger.debug

		body1rect=pygame.Rect( (body1.rect.left, body1.rect.top) , (body1.rect.w, body1.rect.h) )
		body2rect=pygame.Rect( (body2.rect.left, body2.rect.top) , (body2.rect.w, body2.rect.h) )

		contact_surface_x=(body1rect.w+body2rect.w-abs(body1rect.left-body2rect.left)-abs(body1rect.right-body2rect.right))/2
		contact_surface_y=(body1rect.h+body2rect.h-abs(body1rect.bottom-body2rect.bottom)-abs(body1rect.top-body2rect.top))/2

		dprint("Colision: {} - {} ==> Dx: {} - Dy: {} - ".format(body1.id, body2.id, contact_surface_x, contact_surface_y))

		# Em X
		if contact_surface_x<contact_surface_y:
			#print("Colisão em X")
			if   body1rect.left<body2rect.left and body1rect.right<body2rect.right:
				#print("DOWN!!")
				body2rect.left=body1rect.right
				body2.x=body2rect.x

				body1.velocity=(0, body1.velocity[1])
				body2.velocity=(0, body2.velocity[1])
			elif body1rect.left<body2rect.right and body2rect.right<body1rect.right:
				#print("UP!!")
				body2rect.right=body1rect.left
				body2.x=body2rect.x

				body1.velocity=(0, body1.velocity[1])
				body2.velocity=(0, body2.velocity[1])
		# Em Y
		elif contact_surface_x>contact_surface_y:
			#print("Colisão em Y")
			if   body1rect.top<body2rect.top:
				#dprint("DOWN!!")
				body2rect.top=body1rect.bottom
				body2.y=body2rect.y

				body1rect.bottom=body2rect.top
				body1.y=body1rect.y

				body1.velocity=(body1.velocity[0], 0)
				body2.velocity=(body2.velocity[0], 0)
			elif body1rect.bottom>body2rect.bottom:
				#dprint("UP!!")
				body1rect.bottom=body2rect.bottom+body1rect.h
				#body1rect.y=body1.y
				body1.y=body1rect.y-1

				body2rect.bottom=body1rect.top
				body2.y=body2rect.y

				body1.velocity=(body1.velocity[0], 0)
				body2.velocity=(body2.velocity[0], 0)

