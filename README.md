# Arena

Jogo de luta em 2D feito em python e pygame

Baixar o código
=================================================

Para baixar o código fonte, utilizamos o git e o endereço do repositório atual. O endereço do repositório é:

```
https://bitbucket.org/psycho_mantys/arena.git
```

Você pode usar qualquer cliente gráfico do git, mas se você tiver em um ambiente de linha de comando, resumidamente você pode fazer:

```
git clone https://bitbucket.org/psycho_mantys/arena.git
```

Criar ambiente de desenvolvimento
=================================================

Para configurar o ambiente de de desenvolvimento, vamos usar o ```virtualenv```.

Para mais detalhes, consulte a documentação do virtualenv.

O virtualenv só deve ser utilizado uma vez no inicio depois de baixar o código do repositório. Para isso, em um ambiente de linha de comando e considerando que, você deve digitar:

```
cd arena
virtualenv usr/
```

Esse código vai criar um diretório chamado user no diretório do código fonte com as ferramenta do python.

Caso o python do seu sistema não seja por padrão o usado no projeto(no caso o python3), você deve especificar a versão do python no ```virtualenv```, executando no lugar das linhas acima o seguinte:

```
cd arena
virtualenv -p python3 usr/
```


Entrando no ambiente virtual do python
--------------------------------------------------------

Para ativar esse ambiente e usar as ferramentas desse ambiente, execute:

```
source usr/bin/activate
```

Caso você esteja no windows, pode ser:

```
usr/Scripts/activate
```

Saindo do ambiente virtual do python
--------------------------------------------------------

Quando terminar de executar e instalar dependências nesse ambiente, para sair dele use:

```
deactivate
```

Observações
-------------------------------------------------

Em alguns ambiente de linha de comando, o comando ``source`` deve ser substituído por ``.`` ou o comando equivalente daquela plataforma. Para mais informações leia a documentação do virtualenv.

Instalando as dependências de desenvolvimento
=================================================

Para gerenciar as dependências de desenvolvimento, iremos usar o pip.

Entre no ambiente virtual do python do projeto ou instale essas bibliotecas no sistema.

As bibliotecas que são necessárias estão descritas no arquivos ``requirements.txt``. Para instalar de forma automática com o ``pip``, em um ambiente de linha de comando você pode executar:

```
pip install -r requirements.txt
```

Observações
-------------------------------------------------

De vez em quando quase sempre, é bom dar o upgrade no ``pip`` do próprio ambiente.

Pode ser feito com:

```
pip install --upgrade pip
```

Mais informações atualizadas
=================================================

Para mais detalhes atualizados, [consulte este link](https://bitbucket.org/psycho_mantys/arena/wiki/Configurar-ambiente-para-desenvolvimento-e-constru%C3%A7%C3%A3o-do-programa).

