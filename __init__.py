#!/usr/bin/env python

import pygame

import config
import debug

import stage

class Arena(object):
	def __init__(self):
		# Load config from comand line, vars and config file
		self.config=config.Config()

		self.config.debug=debug.Debug(self.config)
		
		self.dt=0
		self.done=False

		successes, failures=pygame.init()
		print("Initializing pygame: {0} successes and {1} failures.".format(successes, failures))

		self.clock=pygame.time.Clock()

		self.screen=pygame.display.set_mode( [self.config.screen_width, self.config.screen_height] )
		pygame.display.set_caption(self.config.window_caption)

		self.stage=stage.STAGE["basic"].Stage(self, self.config)

	def __del__(self):
		pygame.quit()

	def main_loop(self):
		while not self.done:
			self.stage.event_handler()
			self.stage.game_logic(self.dt)
			self.stage.step_physics(self.dt)
			self.stage.render(self.screen)

			self.dt=self.clock.tick(self.config.frame_rate)

if __name__ == "__main__":
	Arena().main_loop()

