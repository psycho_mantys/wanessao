#!/usr/bin/env python

class Group(object):
	def __init__(self):
		self.group_list=[]

	def blit(self, *args):
		for member in self.group_list:
			member.blit(*args)

	def draw(self, *args):
		for member in self.group_list:
			member.draw(*args)

	def update(self, *args):
		for member in self.group_list:
			member.update(*args)

	def add(self, m):
		self.group_list.append(m)

	def remove(self, remove_element):
		if remove_element in self.group_list:
			self.group_list.pop(self.group_list.index(remove_element))

	def print(self):
		for member in self.group_list:
			print(member.id)

	def is_collide(self, rect):
		for member in self.group_list:
			if member.rect.colliderect(rect):
				return True
		return False

