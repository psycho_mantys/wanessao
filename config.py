#!/usr/bin/env python

import argparse
import os

class Config():
	def __init__(self):
		self.round_time = 90.0
		self.sound_volume = 100.0
		self.sound_fadein = 1000
		self.frame_rate = 30.0
		self.window_caption="Arena"

		# Screen dimensions
		self.screen_width = 768
		self.screen_height = 368

		# Ativar debug se > 0
		self.DEBUG=0
		# Objeto para debug
		self.debug=None

		self.load_cli()
		self.load_sys_env()


	def load_cli(self):
		parser = argparse.ArgumentParser()

		screen_size=str(self.screen_width)+"x"+str(self.screen_height)

		parser.add_argument("--volume", type=float, default=self.sound_volume,
			metavar=self.sound_volume, help="Set volume level")
		parser.add_argument("--round-time", type=float, default=self.round_time,
			metavar=self.round_time, help="Set round default duration")
		parser.add_argument("--debug","-d", nargs='?', const=1, default=self.DEBUG,
			metavar='LEVEL', help="Enable debug level mode")
		parser.add_argument("--screen","-s", type=str, default=screen_size,
			metavar=screen_size, help="Set screen size WxH")

		args = parser.parse_args()

		# Set volume
		self.sound_volume = args.volume

		# Set rount time
		self.round_time=args.round_time

		# Enable debug
		self.DEBUG = args.debug

		# Set screen size
		screen_size=args.screen
		self.screen_width=int(screen_size.split('x')[0])
		self.screen_height=int(screen_size.split('x')[1])


	def load_sys_env(self):
		# Set volume
		self.sound_volume=float(os.getenv('SOUND_VOLUME', self.sound_volume))

		# Set default round time
		self.round_time=float(os.getenv('ROUND_TIME', self.round_time))

		# Enable debug
		self.DEBUG=int(os.getenv('DEBUG', self.DEBUG))
		
		# Set screen size
		screen_size=str(self.screen_width)+"x"+str(self.screen_height)
		screen_size=os.getenv('SCREEN_SIZE', screen_size)
		self.screen_width=int(screen_size.split('x')[0])
		self.screen_height=int(screen_size.split('x')[1])


