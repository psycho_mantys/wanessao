#!/usr/bin/env python
#
# Modified and working version of pygame load and display gifs
# Original last version is from a long time ago and dont work.
# 
#
# http://ajaxvm.com/downloads/GIFImage.py
# https://raw.githubusercontent.com/nicksandau/GIFImage_ext/master/GIFImage_ext.py
# https://gist.github.com/BigglesZX/4016539
# 
#


from PIL import Image
import pygame
from pygame.locals import *
import time

def analyse_image(path):
	'''
	Pre-process pass over the image to determine the mode (full or additive).
	Necessary as assessing single frames isn't reliable. Need to know the mode 
	before processing all frames.
	'''
	im = Image.open(path)
	results = {
		'size': im.size,
		'mode': 'full',
	}
	try:
		while True:
			if im.tile:
				tile = im.tile[0]
				update_region = tile[1]
				update_region_dimensions = update_region[2:]
				if update_region_dimensions != im.size:
					results['mode'] = 'partial'
					break
			im.seek(im.tell() + 1)
	except EOFError:
		pass
	return results


class GIFImage(object):
	frames=[]

	def __init__(self, filename):
		self.filename = filename
		self.image = Image.open(filename)
		self.original_size = self.image.size
#Added by NS  *********************
		#self.frames = []
		self.fps_scale = 1
		self.img_scale = 1
#**********************************       

		self.mode = analyse_image(self.filename)['mode']
		self.get_frames()

		self.cur = 0
		self.ptime = time.time()

		self.running = True
		self.breakpoint = len(self.frames)-1
		self.startpoint = 0
		self.reversed = False


	def get_rect(self):
		return pygame.rect.Rect((0,0), self.image.size)


	def get_frames(self):
		#Added by NS  ************
		self.frames=[]
		#*************************

		image=self.image

		palette=image.getpalette()
		last_frame=image.convert('RGBA')

		try:
			while True:
				try:
					duration=image.info["duration"]
				except:
					duration=100

				duration*=.001 #convert to milliseconds!

				#Added by NS  ************
				duration*=self.fps_scale
				#*************************

				'''
				If the GIF uses local colour tables, each frame will have its own palette.
				If not, we need to apply the global palette to the new frame.
				'''
				if not image.getpalette():
					image.putpalette(palette)

				new_frame=Image.new('RGBA', image.size)

				'''
				Is this file a "partial"-mode GIF where frames update a region of a different size to the entire image?
				If so, we need to construct the new frame by pasting it on top of the preceding frames.
				'''
				if self.mode == 'partial':
					new_frame.paste(last_frame)

				new_frame.paste(image, (0,0), image.convert('RGBA'))

				pygame_image = pygame.image.fromstring(new_frame.tobytes(), new_frame.size, new_frame.mode)
				pygame_image=pygame_image.convert_alpha()
				if "transparency" in image.info:
					pygame_image.set_colorkey( pygame_image.unmap_rgb(image.info["transparency"]) )

				self.frames.append([pygame_image, duration])

				last_frame=new_frame
				image.seek(image.tell()+1)

		except EOFError:
			pass

	def update(self,*args):
		pass

	def blit(self, screen, pos=(0, 0)):
		return self.render(screen, pos)

	def draw(self, screen, pos=(0, 0)):
		return self.render(screen, pos)

	def render(self, screen, pos):
		if self.running:
			if time.time() - self.ptime > self.frames[self.cur][1]:
				if self.reversed:
					self.cur -= 1
					if self.cur < self.startpoint:
						self.cur = self.breakpoint
				else:
					self.cur += 1
					if self.cur > self.breakpoint:
						self.cur = self.startpoint

				self.ptime = time.time()
		#Added by NS  **************************************
		if self.img_scale == 1:
			surf = self.frames[self.cur][0]
		else:
			surf = pygame.transform.scale(self.frames[self.cur][0],
				(int(self.image.width * self.img_scale),
				int(self.image.height * self.img_scale)))
		screen.blit(surf, pos)
		#screen.blit(self.frames[self.cur][0], pos)
		#***************************************************

	def seek(self, num):
		self.cur = num
		if self.cur < 0:
			self.cur = 0
		if self.cur >= len(self.frames):
			self.cur = len(self.frames)-1

	def set_bounds(self, start, end):
		if start < 0:
			start = 0
		if start >= len(self.frames):
			start = len(self.frames) - 1
		if end < 0:
			end = 0
		if end >= len(self.frames):
			end = len(self.frames) - 1
		if end < start:
			end = start
		self.startpoint = start
		self.breakpoint = end

	def pause(self):
		self.running = False

#added by NS  ********************************
	def next_frame(self):
		if self.running:
			self.pause()
		else:
			self.cur += 1
			if self.cur > self.breakpoint:
				self.cur = self.startpoint

	def prev_frame(self):
		if self.running:
			self.pause()
		else:
			self.cur -= 1
			if self.cur < 0:
				self.cur = self.breakpoint

	def slow_down(self):
		self.fps_scale += .05 if self.fps_scale != .01 else .04
		self.get_frames()
		self.seek(self.cur)

	def speed_up(self):
		if self.fps_scale - .05 <= 0:
			self.fps_scale = .01
		else:
			self.fps_scale -= .25
		self.get_frames()
		self.seek(self.cur)

	def scale(self, scale_factor):
		self.img_scale += scale_factor

	def reset_scale(self):
		self.img_scale = 1
#*********************************************

	def play(self):
		self.running = True

	def rewind(self):
		self.seek(0)
	def fastforward(self):
		self.seek(self.length()-1)

	def get_height(self):
		return self.image.size[1]
	def get_width(self):
		return self.image.size[0]
	def get_size(self):
		return self.image.size
	def length(self):
		return len(self.frames)
	def reverse(self):
		self.reversed = not self.reversed
	def reset(self):
		self.cur = 0
		self.ptime = time.time()
		self.reversed = False

	def copy(self):
		new = GIFImage(self.filename)
		new.running = self.running
		new.breakpoint = self.breakpoint
		new.startpoint = self.startpoint
		new.cur = self.cur
		new.ptime = self.ptime
		new.reversed = self.reversed
		#Added by NS  ****
		new.fps_scale = self.fps_scale
		#*****************
		return new

##def main():
##    pygame.init()
##    screen = pygame.display.set_mode((640, 480))
##
##    hulk = GIFImage("hulk.gif")
##    football = GIFImage("football.gif")
##    hulk2 = hulk.copy()
##    hulk2.reverse()
##    hulk3 = hulk.copy()
##    hulk3.set_bounds(0, 2)
##    spiderman = GIFImage("spiderman7.gif")
##
##    while 1:
##        for event in pygame.event.get():
##            if event.type == QUIT:
##                pygame.quit()
##                return
##
##        screen.fill((255,255,255))
##        hulk.render(screen, (50, 0))
##        hulk2.render(screen, (50, 150))
##        hulk3.render(screen, (50, 300))
##        football.render(screen, (200, 50))
##        spiderman.render(screen, (200, 150))
##        pygame.display.flip()
##
##if __name__ == "__main__":
##    main()

